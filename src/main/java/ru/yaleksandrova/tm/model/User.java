package ru.yaleksandrova.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.util.HashUtil;

import java.util.UUID;

@Getter
@Setter
public class User extends AbstractEntity {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    private Boolean locked = false;

    public User(@NotNull final String login, @NotNull final String password) {
        this.login = login;
        this.passwordHash = password;
    }

    public User() {

    }

}
