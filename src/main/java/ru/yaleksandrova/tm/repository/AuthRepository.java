package ru.yaleksandrova.tm.repository;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.repository.IAuthRepository;

@Getter
@Setter
public class AuthRepository implements IAuthRepository {

    @NotNull
    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(@Nullable final String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
