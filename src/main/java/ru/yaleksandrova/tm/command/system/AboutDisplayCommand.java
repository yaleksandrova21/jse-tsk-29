package ru.yaleksandrova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.command.AbstractCommand;

public final class AboutDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer information";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final String developer = Manifests.read("developer");
        System.out.println("Developer: " + developer);
        @NotNull final String email = Manifests.read("email");
        System.out.println("E-mail: " + email);
    }

}
