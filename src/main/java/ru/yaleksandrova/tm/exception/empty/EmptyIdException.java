package ru.yaleksandrova.tm.exception.empty;

import ru.yaleksandrova.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty");
    }

}
