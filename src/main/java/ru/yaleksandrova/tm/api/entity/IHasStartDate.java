package ru.yaleksandrova.tm.api.entity;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasStartDate {

    @NotNull Date getStartDate();

    void setStartDate(@NotNull Date startDate);

}
