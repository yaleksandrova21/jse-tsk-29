package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.api.IService;
import ru.yaleksandrova.tm.api.repository.IOwnerRepository;
import ru.yaleksandrova.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {
}

